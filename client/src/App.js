import React from 'react';
import "./App.css";
import { BrowserRouter as Router, Route } from 'react-router-dom';
import  Login   from './components/Login';
import  Register  from './components/Register';
import { Landing } from './components/Landing';
import  NavBar  from "./components/Navbar";
import { Profile } from './components/Profile';



function App() {
  return (
    <Router>
      <div className="App">
        <NavBar />
        <Route exact path="/" component={Landing} />
        <div className="container">
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/profile" component={Profile} />         
        </div>
      </div>
    </Router>
  );
}

export default App;
