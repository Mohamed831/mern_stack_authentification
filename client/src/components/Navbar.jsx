import React, { Component } from "react";
import { Link, withRouter, Redirect, useHistory } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";


function NavBar (){
  const history = useHistory();
  
  const logOut = () => {
    localStorage.clear("usertoken");
    history.push('/')
    
  };

 
    const logRegLink = (
      <ul className="navbar">
        <li className="nav-item">
          <Link to="/login" className="link nav-link">
            Login
                 </Link>
        </li>
        <li className="nav-item">
          <Link to="/register" className="link nav-link">
            Register
                 </Link>
        </li>
      </ul>
    );
    const userLink = (
      <ul className="navbar">
        <li className="nav-item">
          <Link to="/profile" className="link nav-link">
            Profile
                 </Link>
        </li>
        <li className="nav-item">
          <Link onClick={logOut} className="link nav-link">
            Logout
                 </Link>
        </li>
      </ul>
    );
    return (
      <div className="header">
        <Link to="/">
          <h3 className="title">MERN AUTH APP</h3>
        </Link>
          {localStorage.usertoken ? userLink : logRegLink}
      </div>      
    );  
}

export default withRouter(NavBar);
