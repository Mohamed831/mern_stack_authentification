import React, { useContext, useState } from "react";
import TextField from "@material-ui/core/TextField";
import Axios from "axios";
import HandleError from "./HandleError";

export class Login extends React.Component {
         state = {
           email: "",
           password: "",
           error: undefined,
         };
        

         onSubmit = async (e) => {
           e.preventDefault();
           try {
             const userData = {
               email: this.state.email,
               password: this.state.password,
             };
             const result = await Axios.post("/users/login", userData);
             localStorage.setItem("usertoken", result.data);  
             this.props.history.push('/profile')          
           } catch (err) {
             err.response.data.msg && this.setState({error: err.response.data.msg});
           }
         };

         handleEmailChange = (event) => {
           this.setState({ email: event.target.value });
         };
         handlePasswordChange = (event) => {
           this.setState({ password: event.target.value });
         };

         render() {
           return (
             <div className="container">
               <form onSubmit={this.onSubmit} className="form">
                 <h3 className="register">Login</h3>
                 {this.state.error && (
                   <HandleError
                     message={this.state.error}
                     clearError={() => this.setState({ error: undefined })}
                   />
                 )}
                 <TextField
                   id="standard-basic"
                   className="input"
                   label="Email"
                   name="email"
                   value={this.state.email}
                   onChange={this.handleEmailChange}
                 />
                 <TextField
                   id="standard-basic"
                   className="input"
                   label="Password"
                   name="password"
                   type="password"
                   value={this.state.password}
                   onChange={this.handlePasswordChange}
                 />
                 <input type="submit" id="btn" value="Login" />
               </form>
             </div>
           );
         }
       }
export default Login;
