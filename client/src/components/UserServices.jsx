import axios from 'axios';


export const register = newUser => {
    const userData = {
        first_name: newUser.first_name,
        last_name: newUser.last_name,
        email: newUser.email,
        password: newUser.password
    }
    return axios
    .post('/users/create', userData)
    .then(res => {
        console.log('Registered!')
    })
}
export const login = user => {
    const userData = {
        email: user.email,
        password: user.password
    }
    return axios
    .post('/users/login', userData)
    .then(res => {
        localStorage.setItem('usertoken', res.data)
        return res.data
    })
    .catch(err => {
        console.log(err)
    })
}
