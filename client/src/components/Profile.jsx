import React, { Component } from 'react'
import jwt_decode from 'jwt-decode'

export class Profile extends Component {
    state = {       
        displayName: '',
        email: ''
    }
    componentDidMount() {
        const token = localStorage.usertoken
        const decoded = jwt_decode(token)
        this.setState({
            displayName: decoded.displayName,           
            email: decoded.email
        })
        console.log(decoded);

    }
    render() {
        return (
          <div className="container">
            <div className="profile">
              <div className="col-sm-8 mx-auto">
                <h3 className="text-center welcom">
                  Welcome {this.state.displayName}
                </h3>
              </div>
              <table className="table">
                <tbody>
                  <tr>
                    <td>
                      <b>Name</b>:
                    </td>
                    <td>{this.state.displayName}</td>
                  </tr>                  
                  <tr>
                    <td>
                      <b>Email</b>:
                    </td>
                    <td>{this.state.email}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        );
    }
}

export default Profile
