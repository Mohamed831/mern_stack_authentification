const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const path = require('path');
const mongoose = require("mongoose");
const sslRedirect = require("heroku-ssl-redirect");
const port = process.env.PORT || 4000;

// enable ssl redirect
app.use(sslRedirect());

app.use(bodyParser.json());
app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

const mongoURI =
  process.env.MONGODB_URI || "mongodb://localhost:27017/MERN_Login";
mongoose
  .connect(mongoURI, { useNewUrlParser: true })
  .then(() => console.log("Database is connected!"))
  .catch((err) => console.log(err));
  
const Users = require("./routes/Users");

if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

app.use("/users", Users);

app.listen(port, () => {
  console.log("Server running on port: " + port);
});
